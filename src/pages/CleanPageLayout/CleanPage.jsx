import React, { Component } from "react";
import { withNamespaces } from "react-i18next";

function CleanPage({ t }) {
  return <div>{t("clean.test")}</div>;
}
export default withNamespaces()(CleanPage);
