import React from "react";
import "./navbar.css";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import { withNamespaces } from "react-i18next";

function NavBar({ t }) {
  return (
    <div>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Navbar.Brand href="/">{t("nav.home")}</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/news">{t("nav.news")}</Nav.Link>
            <Nav.Link href="/about">{t("nav.about")}</Nav.Link>
            <Nav.Link href="/contact">{t("nav.contact")}</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
}
export default withNamespaces()(NavBar);
