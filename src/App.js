import React, {Component, useEffect} from 'react'
import './App.css';
import Navbar from "./layout/navbar";
import AboutPage from "./pages/About";
import ContactPage from "./pages/Contact";
import HomePage from "./pages/Home";
import CleanPage from "./pages/CleanPageLayout/CleanPage";
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import { withNamespaces } from 'react-i18next';
import i18n from './i18n';
import { Button, ButtonGroup } from 'reactstrap';
import Cookies from "universal-cookie";

function App({t}) {

  const cookies = new Cookies();


  useEffect(() => {
    i18n.changeLanguage(cookies.get('lang'));
  }, [])
  
  const changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
    cookies.set('lang', lng, {path: '/'});
  }
  return (
    <div>
    <Router>
      <Navbar/>
          <div className="col-sm-6 col-sm-offset-6">
      <ButtonGroup >
      <Button color="link" active onClick={() => changeLanguage('ro')}>  <i class="ro flag"></i></Button>
      <Button color="link" active onClick={() => changeLanguage('en')}>  <i class="uk flag"></i></Button>
    </ButtonGroup>
    </div>
      <Switch>
      <Route path="/" exact component={HomePage}/>
      <Route path="/about" component={AboutPage}/>
      <Route path="/clean" component={CleanPage}/>
      <Route path="/contact" component={ContactPage}/>
      </Switch>
    </Router>
    </div>

  );
}
export default withNamespaces()(App);
